import csv
from math import sqrt

# lets open up the file and load it up
with open('/home/nbs/data_engineer_challenge/data/sentiment/data.csv', 'rb') as f:
    raw = csv.reader(f, delimiter=',')
    base = [{'artist':r[1], 'gender': r[2], 'sentiment': int(r[3])} for r in raw]
    
# we need to get the distinct artist/gender combos, so we can start totalling
artists = {a['artist']: {'artist':a['artist'], 'gender':a['gender']} for a in base}.values()

# lets initialize all the variables we will need
artist_values={}
for artist in artists:
    artist['positive'] = 0
    artist['negative'] = 0
    artist['neutral'] = 0
    artist_values[artist['artist']] = artist

# now we will actually count the values by artist
for row in base:
    val = 'positive' if row['sentiment'] > 0 else 'neutral' if row['sentiment'] == 0 else 'negative'
    artist_values[row['artist']][val] += 1

# seperate the ladies from the gentlemen
males = filter(lambda x: True if x['gender']=='male' else False, artist_values.values())
females = filter(lambda x: True if x['gender']=='female' else False, artist_values.values())

# Alright, now we can finally start combining them into pairs with total sentiment values!
pairs = []
for lady in females:
    for gent in males:
        pairs.append((lady['artist'], gent['artist'], lady['positive'] + gent['positive'], lady['neutral'] + gent['neutral'], lady['negative'] + gent['negative']))

# now we will find the most "average" sentiment value, as if plotted on a 3d graph:
avg_x = sum(p[2] for p in pairs) / len(pairs) # x axis will be positive sentiment
avg_y = sum(p[3] for p in pairs) / len(pairs) # y axis will be neutral sentiment
avg_z = sum(p[4] for p in pairs) / len(pairs) # z axis will be negative sentiment

# now we will find the distance each pair is from the average (using euclidian distance) and store them all together
pair_distance  = []
for pair in pairs:
    dist = sqrt((pair[2]-avg_x)**2 + (pair[3]-avg_y)**2 + (pair[4]-avg_z)**2)
    pair_distance.append((pair, dist))


for pair in sorted(pair_distance, key = lambda k: k[1], reverse = True)[:10]:
    print '%s, %s -> positive sentiment: %d | neutral sentiment: %d | negative sentiment: %d | distance from average: %d' % (pair[0][0], pair[0][1], pair[0][2], pair[0][3], pair[0][4], pair[1])
