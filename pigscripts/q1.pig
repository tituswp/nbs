-- Load raw data from csv files in project data directory using comma delimiter
raw = LOAD '/home/nbs/data_engineer_challenge/data/sentiment' USING PigStorage(',') AS (user_name:chararray, artist_name:chararray, artist_gender:chararray, sentiment:int);

-- Group all the mention records by artist name
grouped = GROUP raw BY (artist_name, artist_gender);

-- Determine the aggregate sentiment for each artist
counts = FOREACH grouped GENERATE FLATTEN(group) AS (artist_name,artist_gender), SUM(raw.sentiment) AS agg_sentiment;

male = FILTER counts BY artist_gender eq 'male';
female = FILTER counts BY artist_gender eq 'female';

joins = CROSS female, male;
-- DESCRIBE joins;
-- DUMP joins;


result = FOREACH joins GENERATE female::artist_name as female_name, male::artist_name as male_name, ABS(female::agg_sentiment - male::agg_sentiment) as difference;
-- DESCRIBE result;
ordered = ORDER result BY difference DESC;
STORE ordered INTO '/home/nbs/data_engineer_challenge/data/sorted_artist_pairs.csv' USING PigStorage(';');
