-- Load raw data from csv files in project data directory using comma delimiter
raw = LOAD '/home/nbs/data_engineer_challenge/data/sentiment' USING PigStorage(',') AS (user_name:chararray, artist_name:chararray, artist_gender:chararray, sentiment:int);

fem = FILTER raw BY artist_gender eq 'female';
m = FILTER raw BY artist_gender eq 'male';

together = JOIN fem by user_name, m by user_name;
--describe together;
--DUMP together;

grouped = GROUP together BY (fem::artist_name, m::artist_name);
--DUMP grouped;
--DESCRIBE grouped;

agg = FOREACH grouped GENERATE FLATTEN(group) as (female_artist, male_artist), SUM(together.fem::sentiment) + SUM(together.m::sentiment) as cohort_sentiment;
DUMP agg;
--DESCRIBE agg;
