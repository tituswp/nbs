-- Load raw data from csv files in project data directory using comma delimiter
raw = LOAD '/home/nbs/data_engineer_challenge/data/sentiment' USING PigStorage(',') AS (user_name:chararray, artist_name:chararray, artist_gender:chararray, sentiment:int);

-- Group all the mention records by artist name
grouped = GROUP raw BY artist_name;

-- Show the schema for the previous relation (DESCRIBE can be used on any relation)
DESCRIBE grouped;
-- Prints --> grouped: {group: chararray,raw: {(user_name: chararray,artist_name: chararray,artist_gender: chararray,sentiment: int)}}

-- Determine the number of records associated with each artist
counts = FOREACH grouped GENERATE group AS artist_name, COUNT(raw) AS count;

-- Order the results by the number of records per artist
result = ORDER counts BY count DESC;

-- Print the results to stdout
DUMP result;
