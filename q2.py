import csv
from sys import stdin

# lets make artist pairs empty to start, that way if there are any issues loading the file, it will just YES to the first one
artist_pairs = []

# lets keep track of what week we are in, since a year only has 52 (better make a decision before we get there!)
week_num = 52

# open the file we created with pig and populate the artist pairs (using list comprehension here)
with open('/home/nbs/data_engineer_challenge/data/sorted_artist_pairs.csv/part-r-00000', 'rb') as f:
    raw = csv.reader(f, delimiter = ';')
    artist_pairs = [{'female_artist': row[0], 'male_artist': row[1], 'score': int(row[2])} for row in raw]

def calculateYesNo(female_artist, male_artist):
    '''
        We need to find out, statistically, if we are seeing one of the best artist pairs that we can
        We will figure out the best range of artists we should accept based on the number of pairs we havent seen,
            and the number of weeks we have left in the year. If the artist pair exists within that range,
            we should take it and reap our rewards!
    '''
    
    # need to make sure python knows artist_pairs is in the global scope, since I am modifying it within the local function
    global artist_pairs
    num_left = len(artist_pairs)
    top_num = int(num_left / week_num)
    
    # We will sort and slice this guy so we only loop through the top results we should accept. If we find the pair in any of the top results, lets return true and take it!
    for top_result in sorted(artist_pairs, key = lambda k: k['score'], reverse=True)[:top_num+1]: # have to add +1 since slicing discludes the last element
        if top_result['female_artist'] == female_artist and top_result['male_artist'] == male_artist:
            return True
    
    # If we got to here, we didnt find the artist pair, and we want to reject it.
    # We need to remove the pair from our list of available pairs so we dont calulate with it next time
    # using list comprehension again, could have done filter but might be overkill to put in a lambda that is just an if statement
    artist_pairs = [pair for pair in artist_pairs if pair['female_artist'] != female_artist and pair['male_artist'] != male_artist]
    
    # Lets return false since we didnt find our pair in the top results
    return False

# alright, lets read through our list in standard in. Making the assumption that we are just going to read one by one, all from a single stdin
for line in stdin:
    female_name, male_name = line.split(',')[0], line.split(',')[1].split('\n')[0] # did this just in case there is a newline
    
    if week_num == 0 or len(artist_pairs) <= 1:
        print 'yes: %s, %s' % (female_name, male_name)
        break
    if calculateYesNo(female_name, male_name):
        print 'yes: %s, %s' % (female_name, male_name)
        break
    else:
        print 'no: %s, %s' % (female_name, male_name)
        week_num -= 1

